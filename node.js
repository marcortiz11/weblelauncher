
var http = require('http');
var io = require('socket.io').listen(2210);
var fs = require('fs');

http.createServer(function(req, res){
	fs.readFile("./index.html", function(err, data){
		if (err){
			res.writeHead(500);
			res.end("Error loading index.html");
		}
			res.writeHead(200, {"Content-Type":"text/html"});
			res.end(data);
	});
}).listen(2211);

io.sockets.on('connection', function(socket){
	socket.on("change_page", function(data){
		console.log(data[0]);
		io.sockets.emit("change_page_to",[data[0], data[1]]);
	});
});